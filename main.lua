-- _AUTO_RELOAD_DEBUG = true
song = nil
tool = renoise.tool()

require('commands')
require('actions')
require('gui')

function letsGo ()
    song = renoise.song()
    init()
end

-- function goodnight ()
--     tool = nil
--     song = nil
-- end

tool.app_new_document_observable:add_notifier(letsGo)
-- tool.app_release_document_observable:add_notifier(goodnight)

tool:add_keybinding { name = "Global:tool:vi-noise toggle",  invoke = toggleGUI }
