emode = nil
-- count = ""
command = ""
last_cmd = ""
pause_cmd = {action = nil, args = {}, locked = false}
confirm = false
last_status = ""
timeout = 2000
key_wait = 500
patterns = {
    "^%d*[hjklHLuU^$]$",
    "^%d*[x]$",
    "^[^:]*gg$",
    "^[^:]*G$",
    "CR$",
    "^0$"
}
alter = false
gui = nil
view = nil
lock_focus = nil

cmd_list = {
    ["0"] = { key="0", kind= "count",
        callback = function (rpt)
            buildCmd(cmd_list["0"].key, cmd_list["0"].kind, rpt)
        end
    },

    ["1"] = { key="1", kind="count",
        callback = function (rpt)
            buildCmd(cmd_list["1"].key, cmd_list["1"].kind, rpt)
        end
    },

    ["2"] = { key="2", kind="count",
        callback = function (rpt)
            buildCmd(cmd_list["2"].key, cmd_list["2"].kind, rpt)
        end
    },

    ["3"] = { key="3", kind="count",
        callback = function (rpt)
            buildCmd(cmd_list["3"].key, cmd_list["3"].kind, rpt)
        end
    },

    ["4"] = { key="4", kind="count",
        callback = function (rpt)
            buildCmd(cmd_list["4"].key, cmd_list["4"].kind, rpt)
        end
    },

    ["5"] = { key="5", kind="count",
        callback = function (rpt)
            buildCmd(cmd_list["5"].key, cmd_list["5"].kind, rpt)
        end
    },

    ["6"] = { key="6", kind="count",
        callback = function (rpt)
            buildCmd(cmd_list["6"].key, cmd_list["6"].kind, rpt)
        end
    },

    ["7"] = { key="7", kind="count",
        callback = function (rpt)
            buildCmd(cmd_list["7"].key, cmd_list["7"].kind, rpt)
        end
    },

    ["8"] = { key="8", kind="count",
        callback = function (rpt)
            buildCmd(cmd_list["8"].key, cmd_list["8"].kind, rpt)
        end
    },

    ["9"] = { key="9", kind="count",
        callback = function (rpt)
            buildCmd(cmd_list["9"].key, cmd_list["9"].kind, rpt)
        end
    },

    ["a"] = { key="a", kind="operator",
        callback = function (rpt)
            buildCmd(cmd_list["a"].key, cmd_list["a"].kind, rpt)
        end
    },

    ["b"] = { key="b", kind="operator",
        callback = function (rpt)
            buildCmd(cmd_list["b"].key, cmd_list["b"].kind, rpt)
        end
    },

    ["c"] = { key="c", kind="operator",
        callback = function (rpt)
            buildCmd(cmd_list["c"].key, cmd_list["c"].kind, rpt)
        end
    },

    ["d"] = { key="d", kind="operator",
        callback = function (rpt)
            buildCmd(cmd_list["d"].key, cmd_list["d"].kind, rpt)
        end
    },

    ["e"] = { key="e", kind="operator",
        callback = function (rpt)
            buildCmd(cmd_list["e"].key, cmd_list["e"].kind, rpt)
        end
    },

    ["f"] = { key="f", kind="operator",
        callback = function (rpt)
            buildCmd(cmd_list["f"].key, cmd_list["f"].kind, rpt)
        end
    },

    ["g"] = { key="g", kind="operator",
        callback = function (rpt)
            buildCmd(cmd_list["g"].key, cmd_list["g"].kind, rpt)
        end
    },
    ["h"] = { key="h", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["h"].key, cmd_list["h"].kind, rpt)
        end
    },

    ["i"] = { key="i", kind="modal",
        callback = function (rpt)
            buildCmd(cmd_list["i"].key, cmd_list["i"].kind, rpt)
        end
    },

    ["j"] = { key="j", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["j"].key, cmd_list["j"].kind, rpt)
        end
    },

    ["k"] = { key="k", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["k"].key, cmd_list["k"].kind, rpt)
        end
    },

    ["l"] = { key="l", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["l"].key, cmd_list["l"].kind, rpt)
        end
    },

    ["m"] = { key="m", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["m"].key, cmd_list["m"].kind, rpt)
        end
    },

    ["n"] = { key="n", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["n"].key, cmd_list["n"].kind, rpt)
        end
    },

    ["o"] = { key="o", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["o"].key, cmd_list["o"].kind, rpt)
        end
    },

    ["p"] = { key="p", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["p"].key, cmd_list["p"].kind, rpt)
        end
    },

    ["q"] = { key="q", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["q"].key, cmd_list["q"].kind, rpt)
        end
    },

    ["r"] = { key="r", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["r"].key, cmd_list["r"].kind, rpt)
        end
    },

    ["s"] = { key="s", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["s"].key, cmd_list["s"].kind, rpt)
        end
    },

    ["t"] = { key="t", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["t"].key, cmd_list["t"].kind, rpt)
        end
    },

    ["u"] = { key="u", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["u"].key, cmd_list["u"].kind, rpt)
        end
    },

    ["v"] = { key="v", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["v"].key, cmd_list["v"].kind, rpt)
        end
    },

    ["w"] = { key="w", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["w"].key, cmd_list["w"].kind, rpt)
        end
    },

    ["x"] = { key="x", kind="change",
        callback = function (rpt)
            buildCmd(cmd_list["x"].key, cmd_list["x"].kind, rpt)
        end
    },

    ["y"] = { key="y", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["y"].key, cmd_list["y"].kind, rpt)
        end
    },

    ["z"] = { key="z", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["z"].key, cmd_list["z"].kind, rpt)
        end
    },

    ["A"] = { key="A", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["A"].key, cmd_list["A"].kind, rpt)
        end
    },

    ["B"] = { key="B", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["B"].key, cmd_list["B"].kind, rpt)
        end
    },

    ["C"] = { key="C", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["C"].key, cmd_list["C"].kind, rpt)
        end
    },

    ["D"] = { key="D", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["D"].key, cmd_list["D"].kind, rpt)
        end
    },

    ["E"] = { key="E", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["E"].key, cmd_list["E"].kind, rpt)
        end
    },

    ["F"] = { key="F", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["F"].key, cmd_list["F"].kind, rpt)
        end
    },

    ["G"] = { key="G", kind="operator",
        callback = function (rpt)
            buildCmd(cmd_list["G"].key, cmd_list["G"].kind, rpt)
        end
    },

    ["H"] = { key="H", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["H"].key, cmd_list["H"].kind, rpt)
        end
    },

    ["I"] = { key="I", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["I"].key, cmd_list["I"].kind, rpt)
        end
    },

    ["J"] = { key="J", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["J"].key, cmd_list["J"].kind, rpt)
        end
    },

    ["K"] = { key="K", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["K"].key, cmd_list["K"].kind, rpt)
        end
    },

    ["L"] = { key="L", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["L"].key, cmd_list["L"].kind, rpt)
        end
    },

    ["M"] = { key="M", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["M"].key, cmd_list["M"].kind, rpt)
        end
    },

    ["N"] = { key="N", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["N"].key, cmd_list["N"].kind, rpt)
        end
    },

    ["O"] = { key="O", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["O"].key, cmd_list["O"].kind, rpt)
        end
    },

    ["P"] = { key="P", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["P"].key, cmd_list["P"].kind, rpt)
        end
    },

    ["Q"] = { key="Q", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["Q"].key, cmd_list["Q"].kind, rpt)
        end
    },

    ["R"] = { key="R", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["R"].key, cmd_list["R"].kind, rpt)
        end
    },

    ["S"] = { key="S", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["S"].key, cmd_list["S"].kind, rpt)
        end
    },

    ["T"] = { key="T", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["T"].key, cmd_list["T"].kind, rpt)
        end
    },

    ["U"] = { key="U", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["U"].key, cmd_list["U"].kind, rpt)
        end
    },

    ["V"] = { key="V", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["V"].key, cmd_list["V"].kind, rpt)
        end
    },

    ["W"] = { key="W", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["W"].key, cmd_list["W"].kind, rpt)
        end
    },

    ["X"] = { key="X", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["X"].key, cmd_list["X"].kind, rpt)
        end
    },

    ["Y"] = { key="Y", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["Y"].key, cmd_list["Y"].kind, rpt)
        end
    },

    ["Z"] = { key="Z", kind="move",
        callback = function (rpt)
            buildCmd(cmd_list["Z"].key, cmd_list["Z"].kind, rpt)
        end
    },

    [","] = { key=",", kind="ex",
        callback = function (rpt)
            buildCmd(cmd_list[","].key, cmd_list[","].kind, rpt)
        end
    },

    ["."] = { key=".", kind="ex",
        callback = function (rpt)
            buildCmd(cmd_list["."].key, cmd_list["."].kind, rpt)
        end
    },

    ["~"] = { key="~", kind="ex",
        callback = function (rpt)
            buildCmd(cmd_list["~"].key, cmd_list["~"].kind, rpt)
        end
    },

    ["`"] = { key="`", kind="ex",
        callback = function (rpt)
            buildCmd(cmd_list["`"].key, cmd_list["`"].kind, rpt)
        end
    },

    [":"] = { key=":", kind="ex",
        callback = function (rpt)
            buildCmd(cmd_list[":"].key, cmd_list[":"].kind, rpt)
        end
    },

    ["!"] = { key="!", kind="ex",
        callback = function (rpt)
            buildCmd(cmd_list["!"].key, cmd_list["!"].kind, rpt)
        end
    },

    ["@"] = { key="@", kind="ex",
        callback = function (rpt)
            buildCmd(cmd_list["@"].key, cmd_list["@"].kind, rpt)
        end
    },

    ["$"] = { key="$", kind="ex",
        callback = function (rpt)
            buildCmd(cmd_list["$"].key, cmd_list["$"].kind, rpt)
        end
    },

    ["%"] = { key="%", kind="ex",
        callback = function (rpt)
            buildCmd(cmd_list["%"].key, cmd_list["%"].kind, rpt)
        end
    },

    ["^"] = { key="^", kind="ex",
        callback = function (rpt)
            buildCmd(cmd_list["^"].key, cmd_list["^"].kind, rpt)
        end
    },

    ["/"] = { key="/", kind="ex",
        callback = function (rpt)
            buildCmd(cmd_list["/"].key, cmd_list["/"].kind, rpt)
        end
    },

    ["?"] = { key="?", kind="ex",
        callback = function (rpt)
            buildCmd(cmd_list["?"].key, cmd_list["?"].kind, rpt)
        end
    },

    ["back"] = { key="back", kind="erase",
        callback = function (rpt)
            buildCmd(cmd_list["back"].key, cmd_list["back"].kind, rpt)
        end
    },

    ["del"] = { key="del", kind="erase",
        callback = function (rpt)
            buildCmd(cmd_list["del"].key, cmd_list["del"].kind, rpt)
        end
    },

    ["tab"] = { key="tab", kind="tab",
        callback = function (rpt)
            buildCmd(cmd_list["tab"].key, cmd_list["tab"].kind, rpt)
        end
    },

    ["return"] = { key="CR", kind="return",
        callback = function (rpt)
            buildCmd(cmd_list["return"].key, cmd_list["return"].kind, rpt)
        end
    }
}

function resetCmd ()
    command = ""
    if gui and gui.visible then view.views.command.text=command return end
    if tool:has_timer(resetCmd) then
        tool:remove_timer(resetCmd)
    end
end

function makeTimeout ()
    if tool:has_timer(resetCmd) then
        tool:remove_timer(resetCmd)
    end
    tool:add_timer(resetCmd, timeout)
end

function dispatch ()
    print("dispatch")
    if command:find("^0?%d*[ghjklGHL^$]?g?$") then
        doMove(command)
    elseif command:find("^%d*[x]$") then
        doChange(command)
    elseif command:find("^:%w*") then
        doEx(command)
    elseif command:find("^%d*[uU]$") then
        undo(command)
    else
        resetCmd()
    end
end

function isCmdDone ()
    for i = 1, #patterns do
        if command:find(patterns[i]) then
            dispatch()
        end
    end
end

function focusEditor()
    lock_focus = renoise.app().window.lock_keyboard_focus
    renoise.app().window.lock_keyboard_focus = false
    renoise.app().window.lock_keyboard_focus = true
end

function resetFocus ()
    if lock_focus ~= nil then
        renoise.app().window.lock_keyboard_focus = lock_focus
    end
end

function buildCmd (key, kind, rpt)
    makeTimeout()
    if kind == "erase" and #command ~= 0 then
        command = command:sub(1, -2)
        return
    end
    if #command == 0 and kind == "modal" and not rpt then
        if key == "i" then
            song.transport.edit_mode = true
            focusEditor()
        end
        return
    end
    if not emode then
        if rpt and key:find("^[hjklHL]$") then
            command = key
            dispatch()
            return
        elseif not rpt then
            command = command .. key
            if command:find("^:?%w*") then
            print(command)
                isCmdDone()
            else
                dispatch()
            end
        end
    end
end
