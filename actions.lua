function init()
    emode = song.transport.edit_mode
    song.transport.edit_mode_observable:add_notifier(setEmode)
end

function setEmode ()
    emode = song.transport.edit_mode
    if not emode and gui and gui.visible then
        gui:show()
        resetFocus()
    end
end

function toggleGUI (rpt)
    if rpt then return end
    if gui and gui.visible then gui:close(); gui = nil; return end
    resetCmd()
    view = makeGUI()
end

function save ()
    renoise.app():save_song()
end

function load ()
    renoise.app():load_song(renoise.app():open_path("~"))
end

function undo (cmd)
    local start, stop, count = cmd:find("^(%d+)")
    if count then count = tonumber(count) else count = 1; stop = 0 end
    local key = cmd:sub(stop + 1, stop + 2)
    if key == "u" then
        while count > 0 do
            if not song:can_undo() then break end
            song:undo()
            count = count - 1
        end
    elseif key == "U" then
        while count > 0 do
            if not song:can_redo() then break end
            song:redo()
            count = count - 1
        end
    end
    resetCmd()
end

function linesInSong ()
    local totalLines = 0
    for i, _ in ipairs(song.sequencer.pattern_sequence) do
        totalLines = totalLines + song:pattern(i).number_of_lines
    end
    return totalLines
end

function currentLineInSong ()
    local pos = song.transport.edit_pos
    local line = 0
    local i = 1
    while i < pos.sequence do
        line = line + song:pattern(i).number_of_lines
        i = i + 1
    end
    return line + pos.line
end

function parseLineCount (count)
    -- #song.sequencer.pattern_sequence
    -- for i, p in ipairs(renoise.song().sequencer.pattern_sequence) do print(i, p) end
    -- renoise.song():pattern(index).number_of_lines
    local totalLines = linesInSong()
    if count < 1 then return 1, 1 end
    if count > totalLines then
        local seq = #song.sequencer.pattern_sequence
        local line = song:pattern(seq).number_of_lines
        return line, seq
    end
    local index = 1
    local plines = song:pattern(index).number_of_lines
    while count > 0 do
        if count - plines < 1 then
            return count, index
        end
        count = count - plines
        index = index + 1
        plines = song:pattern(index).number_of_lines
    end
end

function doMove (cmd)
    print("move")
    local _, stop, count = cmd:find("([1-9]%d*)C?R?")
    if count then count = tonumber(count) else count = 1; stop = 0 end
    local key = cmd:sub(stop + 1)
    local mult = 1
    local pos = song.transport.edit_pos
    if key == "k" or key == "j" then
        if key == "k" then mult = mult * -1 end
        pos.line, pos.sequence = parseLineCount(currentLineInSong() + (count * mult))
    elseif key == "h" or key == "l" then
        if key == "h" then mult = mult * -1 end
        local jump = count * mult
        local track = song.selected_track_index
        if track + jump < 1 then
            song.selected_track_index = 1
        elseif track + jump > #song.tracks then
            song.selected_track_index = #song.tracks
        else
            song.selected_track_index = song.selected_track_index + jump
        end
-- function setColumn (count, key) -- this is bound to crash with excess count
    elseif key == "H" or key == "L" then
        if key == "H" then mult = mult * -1 end
        if count == 0 then count = 1 end
        local jump = count * mult
        local track = song.selected_track_index
        local n_cols = song.tracks[track].visible_note_columns
        local fx_cols = song.tracks[track].visible_effect_columns
        local col = song.selected_note_column_index
        if col + jump < 1 then
            song.selected_note_column_index = 1
        elseif col + jump > n_cols then
            song.selected_effect_column_index = (col + jump) - n_cols --add spill arithmetic here
        else
            song.selected_note_column_index = song.selected_note_column_index + jump
        end
    elseif key == "gg" then
        if count == 1 then
            pos.sequence = 1
            pos.line = 1
        else
            pos.line, pos.sequence = parseLineCount(count)
        end
    elseif key == "G" then
        if count == 1 then
            pos.sequence = #song.sequencer.pattern_sequence
            pos.line = song:pattern(pos.sequence).number_of_lines
        else
            pos.line, pos.sequence = parseLineCount(count)
        end
    elseif key == "jump" then
        pos.line, pos.sequence = parseLineCount(count)
    elseif key == "0" then
        pos.line = 1
    elseif key == "^" then
        pos.line = 1
    elseif key == "$" then
        pos.line = song:pattern(pos.sequence).number_of_lines
    end
    song.transport.edit_pos = pos
    resetCmd()
end

function doChange (cmd)
    print("change")
    local _, stop, count = cmd:find("([1-9]%d*)")
    if count then count = tonumber(count) else count = 1; stop = 0 end
    local key = cmd:sub(stop + 1)
    local index = song.selected_line_index
    if key == "x" then
        if count == 1 then
            song.selected_line:clear()
        elseif count > 1 then
            local i = 1
            while count > 0 do
                song.selected_line:clear()
                count = count - 1
                song.selected_line_index = index + i
                i = i + 1
            end
        end
    end
    resetCmd()
end

function doEx (cmd)
    cmd = cmd:gsub("^:", "")
    if cmd:find("^%d+CR$") then
        doMove(cmd .. "jump")
        return
    elseif cmd:find("^%d*[u]n?d?o?CR$") then
        undo(cmd)
        return
    elseif cmd:find("^wr?i?t?e?CR$") then
        save()
    elseif cmd:find("^ed?i?t?CR$") then
        -- load()
    end
    resetCmd()
end

