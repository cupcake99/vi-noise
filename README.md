# vi-noise

This is a vi style control layer for the music production software Renoise.


## How it works

The script puts a small window on the screen which captures all keystrokes and allows the user to compose commands which are then translated into instructions that are sent to the main program to execute.

## Installation

```
git clone https://cupcake99@bitbucket.org/cupcake99/vi-noise.git
cd vi-noise && ./pack_xrnx_vi-noise
```
Then drag & drop the xrnx package file on the Renoise window to install.
