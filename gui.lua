function makeGUI(rpt)

    local view = renoise.ViewBuilder()

    local DIALOG_MARGIN = renoise.ViewBuilder.DEFAULT_DIALOG_MARGIN
    local CONTENT_SPACING = renoise.ViewBuilder.DEFAULT_CONTROL_SPACING

    local TEXT_ROW_WIDTH = 240

    local content_view =  view:column {
        margin = DIALOG_MARGIN,
        spacing = CONTENT_SPACING,
        view:vertical_aligner {
            mode = "distribute",
            spacing = CONTENT_SPACING,
            view:row {
                style = "plain",

                view:text {
                    id = "command",
                    width = TEXT_ROW_WIDTH,
                    align = "center",
                    font = "big",
                    text = ""
                }
            },

            view:row {
                uniform = true,

                view:horizontal_aligner {
                    width = TEXT_ROW_WIDTH,
                    spacing = CONTENT_SPACING,
                    mode = "distribute",
                    view:column {
                    style = "panel",
                        view:text {
                            id = "mode",
                            width = ( TEXT_ROW_WIDTH/2 ) - CONTENT_SPACING,
                            align = "left",
                            font = "big",
                            text = "normal"
                        }
                    },

                    view:column {
                    style = "panel",
                        view:text {
                            id = "line",
                            width = ( TEXT_ROW_WIDTH/2 ) - CONTENT_SPACING,
                            align = "right",
                            font = "big",
                            text = "line"
                        }
                    }
                }
            },

            view:row {
                style = "group",

                view:multiline_text {
                    id = "key_text",
                    width = TEXT_ROW_WIDTH,
                    height = 92,
                    font = "big",
                    paragraphs = {
                        "key.name:",
                        "key.modifiers:",
                        "key.character:",
                        "key.note:",
                        "key.repeat:"
                    },
                }
            }
        }
    }

    local function key_handler(dialog, key)

        if emode then
            return key
        else
            if key.modifiers == "option + command" and key.name == "v" then
                toggleGUI()
                return
            end
            if cmd_list[key.character] then
                cmd_list[key.character].callback()
            elseif key.name == 'return' or
                   key.name == 'back' or
                   key.name == 'del' then
                cmd_list[key.name].callback()
            end
            view.views.command.text = command
            view.views.key_text.paragraphs = {
                ("key.name: '%s'"):format(key.name),
                ("key.modifiers: '%s'"):format(key.modifiers),
                ("key.character: '%s'"):format(key.character or "nil"),
                ("key.note: '%s'"):format(tostring(key.note) or "nil"),
                ("key.repeated: '%s'"):format(key.repeated and "true" or "false"),
            }
        end
    end

    -- local opts =  {send_key_repeat=true, send_key_release=false}

    gui = renoise.app():show_custom_dialog("vi-noise", content_view, key_handler)

    return view

end

